package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
)

func main() {

	args := os.Args[1:]

	if len(args) != 2 {
		log.Fatal("Usage: FileServer <dir> <port>")
		return
	}

	dir := http.Dir(args[0])
	port, err := strconv.Atoi(args[1])

	if err != nil {
		log.Fatal("Please use an integer in the port argument")
		return
	}

	fs := http.FileServer(dir)
	http.Handle("/", fs)
	log.Printf("Starting FileServer on port %d", port)

	err = http.ListenAndServe(fmt.Sprintf(":%d", port), nil)
	if err != nil {
		log.Fatal(err)
	}
}
